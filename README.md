

# Overview

The goal of this project is to classify images of dogs by their breed. This is a classic example of image classification. The algorithm will take an image as input and will return a prediction for the breed of the dog. If a human face is detected, the algorithm will provide a prediction for the dog breed that most resembles the face.

This process is split into three separate tasks. First, detect if a dog is present in the images (binary image classification). Second, detect if a human face is present in the image (binary image classification). And, finally, predict the most closely resembling dog breed (multi-task classification). For each task, we train a separate model. When an image is passed into the final pipeline all three steps are called.

Deep learning has significantly advanced the field of image classification over the last few years. Therefore, we will adopt a deep-learning solution. In particular, we will take advantage of Convolution Neural Nets (CNNs) and transfer learning.


## Running the code

1. Clone the repository and navigate to the downloaded folder.
```	
git clone https://gitlab.com/andrew_johnson_melb/dog_breed_classifier.git
cd dog-project
```

2. Download the [dog dataset](https://s3-us-west-1.amazonaws.com/udacity-aind/dog-project/dogImages.zip).  Unzip the folder and place it in the repo, and update the paths in the notebook. 

3. Download the [human dataset](https://s3-us-west-1.amazonaws.com/udacity-aind/dog-project/lfw.zip).  Unzip the folder and place it in the repo, and update the paths in the notebook. 

4. Download the [VGG-16 bottleneck features](https://s3-us-west-1.amazonaws.com/udacity-aind/dog-project/DogVGG16Data.npz) for the dog dataset.  Place it in the repo, at location `../bottleneck_features`.

6. If you are using __Lunix__, to create and activate a new environment.
```bash
	conda env create -f requirements/dog-linux.yml
	conda activate dog-project
```

## Files

The main analysis is contained in the notebook `./notebooks/dog_app.ipynbx`

The requrements are listed in `./requirements/dog-linux.yml`

` extract_bottleneck_features.py` contains a few util functions used in the notebook.


## Data

To train the dog breed classification model, we have access to 8351 labelled images of dogs. These images contain 133 separate dog breeds. This classification task is particularly challenging because a number of the dog breeds as visually quite similar, i.e., humans would have trouble distinguishing these breeds. 

To train the human face detector we have 13233 images of human faces.

## Accuracy Measure

The initial dataset is divided into train/validation/test sets using, respectively, 80%, 10%, and 10% of the full dataset. To measure the performance of the dog-breed classification algorithm we will use the classification accuracy on the test data set. We adopt this measure because there are no large class imbalances within the dataset. Note, a random guess provides an accuracy baseline of 1/133.

## Motivation

Throughout this work, we will take advantage of Convolution Neural Nets and a process called Max Pooling. This is very standard for deep learning image problems. However, it's important to understand why these "modules" or "layers" are so useful. 

The first observation to make is that the image world is translation invariant: a dog looks like a dog regardless of where it is in the image.
CNN's take advantage of this property because the patterns they learn are translation invariant. In particular, they learn a set of convolution, which can be applied anywhere on an image.

The second observation to make is that images contain spatial hierarchies: A human has a face which has eyes and each eye has an iris. By using "max-pooling" we can take advantage of the spatial hierarchies present in images. 

Max-pooling forces the network to look at larger and larger fields of view (as we move through the layers in the network). This causes the network to learn more and more complex representations, i.e., moving from an iris to a face. And, therefore, taking advantage of spatial hierarchies.


# Methodology

## Preprocessing

The following steps are adopted for preprocessing images:

1. The input image is resized to the target size of 224 by 224
2. The image is converted to an array of dimensions (1, 224, 224, 3) -- the 1st dimension indicates the batch and the last represents the colour channel.

When using pre-trained models the following additional steps are required:

3. Reorder the colour channels from RGB to BRG
4. Normalized each pixel to match the values used to normalize the ImageNet dataset

## Detecting Human Faces

To detect human faces we adopt a model from the OpenCV library. In particular, their implementation of Haar feature-based 
cascade classifier. Our test suggests this model is very accurate. 

## Detecting the presence of a dog

To detect if a dog is present in an image we adopt a pre-trained ResNet-50 model. This model was pre-trained on the ImageNet dataset. 
ImageNet is a popular benchmark dataset for cutting edge image classification. It consists of 10 million images and contains 1000 categories. Fortunately, 150 of the categories in ImageNet are dog breeds. Therefore, to detect the presence of the dog in an image, we simply pass an image through ResNet-50 and check if a dog breed category is returned. 
 
In 2012 ResNet won the ImageNet challenge. The key improvement from previous model architectures was the addition of "skip-connections".
These new "layers" allowed the network we be very deep: 152 layers in fact. This is because skip-connections help to avoid the vanishing gradient problem: the inability of information to propagate backwards in the network for many layers.

## Detecting dog breeds

We implement two methods for detecting dog breeds. First, we train the image detection model from scratch. Second, 
we adopt transfer learning to significantly boost the final accuracy. Both use deep-learning solutions.

Model 1: Training from scratch

The architecture we adopt consists of a shallow stack of CNN's and Max-Pooling layers, and then a dense layer with a softmax transformation. This model is effectively a shallow version of VGG19. This was chosen because this architecture has impressive performance relative to its level of complexity.
Because this model was trained from scratch the main consideration was that it is relatively simple and shallow relative to modern
image classification models. The simplicity means that we will be able to train the model in a reasonable amount of time. And, moreover, we will be less likely to overfit the data -- which is a high possibility as the dataset is small.
One way to view the final system is as follows. We can consider the CNN layers a type of 'image feature extraction module". And, the dense layers can then be considered as a module that optimally combines the extracted features. 


## Model 2: Using transfer learning

It's rarely a good idea to train a neural network from scratch. Transfer learning is a very powerful technique used in deep-learning, in particular, in image classification. This is because many of the kernels learnt when training a model on public datasets are transferable to different tasks, e.g., low levels kernels such as edge detectors will be relevant for most image classification talks.

For our model, we used the pre-trained weights from the Inception V3 model. We take the output of the Inception model and then build a neural network to predict dog breeds. Specifically,  we flatten the output to a 1-dimensional vector and then use two densely connected layers, each with 133 hidden layers. The final layer applies a softmax transformation to normalize the output vector.

## Refinement

To find the best architecture for model 1 a number of different network combinations were tested.  This consisted of extending the number of successive CNN layers and trying larger fully connected dense layers. These more complex models tended to a) take to long to train b) overfit the data and c)  give a validation accuracy very close to random.  

Model 2 was refined in two ways. First, different pre-trained models were tested. In particular, VGG19 and Inception V3. The Inception V3 model gave better accuracy on the test set. Secondly, the size and number of fully connected dense layers were fine-tuned. Finally, a dropout layer was added and tested. The optimal architecture was 2 fully connected dense layers with 133 hidden dimensions and no dropout.

# Model Evaluation and Validation

To measure the model's performance we use a train/validation/test split. The robustness of the models' predictions is then evaluated on the test set.

For model 1 we measure a test set classification accuracy of 1.1962%. 

This is very poor because there is not enough data to properly capture the differences between dog breeds. This result motivates the use of transfer learning. 

For model 2 we measure a test set classification accuracy of 65%.

The performance improvement from model 1 is impressive. By leveraging the weights learn by training on ImageNet the performance has improved by a factor of 60. This suggests that a lot of the image information extracted by the CNN part of Inception is relevant for dog breed classification.

# Conclusion

With a limited dataset, identifying dog breeds from scratch is a very challenging problem. Our initial model barely reached 1% accuracy. However,
the weights learnt on larger, more diverse, datasets are very relevant for this task. In particular, by using the weights from the Inception V3 model --
as trained on the ImageNet dataset -- we significantly improved the performance of our model.

One very important component of training image classifiers 
is data augmentation. By augmenting -- rotating, scaling, 
shifting -- the data, one can significantly reduce the
 chance of overfitting. This is because the network will never see the same image twice. This forces the network to only learn generalisable patterns. Data augmentation was not included in the data pipeline implemented above. This extra step could potentially significantly improve the accuracy of our models.